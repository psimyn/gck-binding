package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"os"
)

type Server struct {
	uuid  string
	log   *Log
	conns *Conns
}

func NewServer(uuid string) (*Server, error) {
	log, err := NewSessionLog(uuid)
	if err != nil {
		return nil, err
	}

	conns := NewConnections()

	return &Server{uuid: uuid, log: log, conns: conns}, nil
}

func (s *Server) Start() error {
	// 1. Acquire Unix Socket syscall.SOCK_STREAM type
	// 2. Read output and pipe it to a session log and os.Stdout
	// 3. Read input and write it to a session log and to clients

	listener, _, err := s.socket()
	if err != nil {
		return err
	}
	defer listener.Close()

	go s.serverPipe()

	for {
		conn, err := listener.Accept()
		if err != nil {
			continue
		}

		s.log.Rewind(conn)

		go s.pipeClient(s.conns.Add(conn), conn)
	}
}

func (s *Server) socket() (net.Listener, string, error) {
	sock := fmt.Sprintf("%s.sock", s.uuid)
	os.Remove(sock)

	listener, err := net.Listen("unix", sock)

	return listener, sock, err
}

// pipeClient reads client data and pipes it into standard output
func (s *Server) pipeClient(id int32, c net.Conn) net.Conn {
	reader := bufio.NewReader(c)

	for {
		line, err := reader.ReadBytes('\n')
		if err != nil {
			return s.conns.Remove(id)
		}

		// session write-ahead-log
		s.log.Write(line)
		// pipe to standard output
		os.Stdout.Write(line)

		// write to the rest of connected clients but skip the one that writes
		s.conns.FanoutWithout(id, line)
	}
}

// serverPipe reads standard input and pipes to all clients
func (s *Server) serverPipe() {
	in, err := os.OpenFile("/dev/stdin", os.O_RDONLY|os.O_SYNC, 0644)
	if err != err {
		log.Fatal("could not open stdin for reading")
	}
	defer in.Close()

	// fan-out to all conntected clients
	for {
		b := make([]byte, 1)
		_, err := in.Read(b)
		if err != nil {
			return // stdin closed for reading
		}

		// session write-ahead-log
		s.log.Write(b)

		// write to all clients
		s.conns.Fanout(b)
	}
}
