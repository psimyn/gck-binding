require 'pry'

module GCK
  module Debug
    class Session
      def initialize(tunnel)
        @tunnel = tunnel

        @tunnel.msg("Starting a new debugger session ...")
      end

      def spawn(binding)
        net = Thread.new { @tunnel.connect! }
        input = GCK::Debug::Input.new(@tunnel.input, @tunnel.output)

        Pry.start(binding, input: input, output: @tunnel.output)

        net.join
      end
    end
  end
end
