require 'net/ssh'
require 'io/wait'

module GCK
  module Debug
    class Tunnel
      def initialize(host: 'localhost', user: 'root', password: 'gck', port: 2222)
        @host = host
        @port = port
        @user = user
        @passwd = password

        @input_reader, @input_writer = IO.pipe
        @output_reader, @output_writer = IO.pipe
      end

      def input
        @input_reader
      end

      def output
        @output_writer
      end

      def send(data)
        @output_writer.print(data)
      end

      def msg(msg)
        send("\n#{msg}\n")
      end

      def connect!
        Net::SSH.start(@host, @user, password: @passwd, port: @port) do |ssh|
          main = ssh.open_channel do |channel|
            cmd = channel.exec('/usr/bin/gck-io-mux start') do |ch|
              channel.on_data do |ch, data|
                @input_writer.write(data)

                if data.rstrip == 'exit'
                  channel.close
                end
              end

              channel.on_extended_data do |ch, type, data|
                @input_writer.write(data)
              end

              channel.on_process do |ch|
                n = @output_reader.nread

                if n > 0
                  data = @output_reader.read(n)
                  ch.send_data(data)
                end
              end
            end
          end

          ssh.loop(0.1)
        end
      end
    end
  end
end
